return {
  statusCode: 404,
  headers: { "Content-Type": "application/json" },
  body: {
    ok: false,
    error: "Endpoint not found",
  },
};
