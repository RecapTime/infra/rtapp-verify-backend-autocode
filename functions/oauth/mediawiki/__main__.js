const { getRedirectUrl } = require("../../../lib/oauth/mw.js");

module.exports = async (state = "abc123") => {
  if (context.params.state) {
    return {
      headers: { Location: getRedirectUrl(context.params.state) },
      statusCode: 302,
      body: Buffer.from(""),
    };
  } else {
    return {
      headers: {
        Location:
          "https://recaptime.miraheze.org/wiki/Portal:NoStateCodeDetected",
      },
      statusCode: 302,
      body: Buffer.from(""),
    };
  }
};
