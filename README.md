# Verification Endpoint API backend

[![Open in gitpod](https://img.shields.io/badge/Gitpod-ready--to--code-908a85?logo=gitpod)](https://gitpod.io/#https://gitlab.com/RecapTime/infra/rtapp-verify-backend-autocode)

This is our source code for the Verification Endpoint API handler and Discord bot running on Autocode, usually synchorized every hour through GitLab CI.

API calls from `verify.rtapp.<tk|io>` domain will be trnsparently redirected to the production release at `rtapp-verify-backend.recaptime.autocode.gg`.

## Development

Requires Node.js 14+ and Autocode CLI if you want to do local development or even deployment to development/production environments on Autocode.

### Setup

1. `npm install`
2. `npm start` to run an local FunctionScript gateway with hot reload included.
3. `curl -i http://localhost:8170/recaptime/rtapp-verify-backend/ping` to check if everything is ready

### Deployment

In order for this to work on your Autocode account, you need to update the value of `name` in `stdlib.json` to point to your namespace instead of pointing at `recaptime`. You also need to build your own `env.json` file before running `lib up dev`.

1. Authenticate with `lib login`.
2. Prepare `env.json` using the template from `env.example.json`. DO NOT COMMIT!
3. Update the value `name` to point to your namespace (e.g. `username/your-app-here`).
4. Deploy with `lib up dev`

## Getting help

* Discord: For Autocode users who are in [the official Autocode Discord sevrer](https://discord.gg/autocode) or Miraheze users on Miraheze Discord, just ping Andrei Jiroh (`Andrei Jiroh#7159`). You can also optionally [join Recap Time Squad's Discord server](https://discord.gg/5dURUcM) and ask for help under `#rtapp-verify-bot` channel.
* Issue tracker: We track issues on <https://gitlab.com/RecapTime/verify/issues> instead.

## Related Projects

* [Main project repo](https://gitlab.com/RecapTime/verify) ([GitHub mirror](https://github.com/RecapTime/verify) is available through we use issue forms) - all the verification metadata are being stored publicly, through some private data are held in an MongoDB database instead.

## Credits

* [Hakiki](https://discord.gg/hakiki) for the landing page guide: <https://hakiki.api.stdlib.com/blog@dev/landingpage.html>
