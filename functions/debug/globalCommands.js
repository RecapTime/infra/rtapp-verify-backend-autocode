const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });

module.exports = async () => {
  return lib.discord.commands["@0.1.0"].list();
};
