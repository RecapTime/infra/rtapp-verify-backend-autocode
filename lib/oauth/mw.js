const { loadEnvFromFile } = require("../secrets");
loadEnvFromFile();

const authroot = process.env.MW_OAUTH_ROOT;
const authorize = `${authroot}/oauth2/authorize`;
const token = `${authroot}/oauth2/access_token`;
const profileurl = `${authroot}/oauth2/resource/profile`;
const axios = require("axios");

function getRedirectUrl(oauthState) {
  let callback = encodeURIComponent(process.env.MW_OAUTH_CALLBACK);
  let str = `client_id=${process.env.MW_OAUTH_CLIENT_ID}&response_type=code&request_url=${callback}&state=${oauthState}`;
  return `${authorize}?${str}`;
}

/**
 * Gets an OAuth2 access token from specific server
 * @param {"string"} respCode
 * @param {"string"} oauthState
 * @returns
 */
function getToken(respCode, oauthState) {
  const oauthResult = axios(token, {
    method: "post",
    params: {
      grant_type: "authorization_code",
      client_id: process.env.MW_OAUTH_CLIENT_ID,
      client_secret: process.env.MW_OAUTH_CLIENT_SECRET,
      redirect_url: process.env.MW_OAUTH_CALLBACK,
      code: respCode,
      state: oauthState,
    },
  });
  console.log(oauthResult);
  return oauthResult;
}

function getMe(token) {
  //
}

module.exports = {
  getRedirectUrl,
  getToken,
  getMe,
};
