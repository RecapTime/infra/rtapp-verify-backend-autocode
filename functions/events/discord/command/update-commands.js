// authenticates you with the API standard library
// type `await lib.` to display API autocomplete
const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });

if (context.params.event.member.permission_names.includes("MANAGE_GUILD")) {
  return lib.discord.interactions["@1.0.1"].followups.ephemeral.create({
    token: `${context.params.event.token}`,
    content: `<@${context.params.event.member.user.id}>,  we're updated the slash commands for this server. If you still seeing old data, wait for up to an hour before retrying.\n\nIn case that didn't work, join the support server at https://discord.gg/5dURUcM under \`recaptime-support\` channel and let us know.`,
  });
}
