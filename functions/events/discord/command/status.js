// authenticates you with the API standard library
// type `await lib.` to display API autocomplete
const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });

// our admin role ID and the guild ID for perm checks.
let role = `${process.env.DISCORD_ROLE_ID}`;
let serverInfo = await lib.discord.guilds["@0.2.3"].retrieve({
  guild_id: `${process.env.DISCORD_GUILD_ID}`,
  with_counts: false,
});
let serverOwner = serverInfo.owner_id;

// if they have admin perms and sent from an guild we specified earlier
// or if they're the server owner themselves.
if (
  context.params.event.member.roles.includes(role) ||
  context.params.event.member.user.id === serverOwner
) {
  /* fetch options from the payload */
  let status = context.params.event.data.options[0].value;
  let type = context.params.event.data.options[1].value;
  let name = context.params.event.data.options[2].value;
  let url = context.params.event.data.options[3];

  // Converting variable from URL option
  if (!url) {
    url = "";
  } else {
    url = url.value;
  }

  await lib.discord.users["@0.1.5"].me.status.update({
    activity_name: name,
    activity_type: type,
    status: status,
    url: url,
  });

  // TODO: Make this message invokation visible to sender only.
  return await lib.discord.channels["@0.2.0"].messages.create({
    channel_id: `${context.params.event.channel_id}`,
    content: ``,
    tts: false,
    embeds: [
      {
        type: "rich",
        title: `✅ Status message successfully updated`,
        description: `Your client should shown the changes immediately. Otherwise, please restart your Discord client.`,
        color: 0x00ff4c,
      },
    ],
    response_type: "CHANNEL_MESSAGE_WITH_SOURCE",
  });
} else {
  // send a error message about missing perms and also pinging people with that role
  let deny = await lib.discord.channels["@0.2.0"].messages.create({
    channel_id: `${context.params.event.channel_id}`,
    content: ``,
    tts: false,
    embeds: [
      {
        type: "rich",
        title: `❌ You don't have enough permissions to do this!`,
        description: `<@${context.params.event.member.user.id}>, please ask a <@&${process.env.DISCORD_ROLE_ID}> to update te bot's status.`,
        color: 0xff0000,
      },
    ],
    response_type: "CHANNEL_MESSAGE_WITH_SOURCE",
  });
}
