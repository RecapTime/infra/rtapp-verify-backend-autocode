const { getToken, getMe } = require("../../../lib/oauth/mw.js");
const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });

module.exports = async (state, code) => {
  if (state) {
    if (state == "abc123") {
      return getToken(code, state);
    }
  } else {
    return {
      headers: {
        Location:
          "https://recaptime.miraheze.org/wiki/Portal:NoStateCodeDetected",
      },
      statusCode: 302,
      body: Buffer.from(""),
    };
  }
};
