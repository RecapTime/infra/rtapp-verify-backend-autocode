const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const index = new Schema({
  name: String,
  realName: {
    type: String,
    default: null
  },
  socials: {
    discord: [
      {
        id: Number,
        createdOn: Date,
        discriminator: String
      }
    ],
    twitter: [
      {
        username: String,
        id: Number,
        createdOn: Date
      }
    ],
    miraheze: [{
      username: String,
      id: Number,
      createdOn: Date
    }],
    telegram: [{
      username: {
        type: String,
        default: null
      },
      id: Number
    }]
  },
  type: "content_creator" || "community" || "entity",
  annonations: Object,
});

module.exports = {
  index,
};
