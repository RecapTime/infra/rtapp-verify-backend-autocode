// authenticates you with the API standard library
// type `await lib.` to display API autocomplete
const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });
let ghFormTemplateLink = `https://github.com/${process.env.GH_NAMESPACE}/${process.env.GH_REPO_SLUG}/issues/new?assignees=ajhalili2006%2CRecapTimeSquad&labels=request-forms%3Anew-entries%2Cenhancement&template=verify-an-entry.yml&title=Request+to+verify+Entry+Name+Here`;
const { getEmbedTemplate } = require("../../../../lib/embeds/verify");

msgTemplate = {
  content: `The embed below contains the instructions you needed to follow an the button links you need. When prompted for your Discord ID for verification, your ID can be copied from the account settings OR your profile when you enable \n\n⚠️**IMPORTANT: Never share your personal data, including your goernment IDs in the issue tracker.** An maintainer may ask you to provide these for the second stage of the verification process after getting the semi-verified status in a private setting.`,
  tts: false,
  components: [
    {
      type: 1,
      components: [
        {
          style: 5,
          label: `Submit new request`,
          url: `https://github.com/${process.env.GH_NAMESPACE}/${process.env.GH_REPO_SLUG}/issues/new?assignees=ajhalili2006%2CRecapTimeSquad&labels=request-forms%3Anew-entries%2Cenhancement&template=verify-an-entry.yml&title=Request+to+verify+Entry+Name+Here`,
          disabled: false,
          type: 2,
        },
        {
          style: 5,
          label: `Recommended: Find existing issues first`,
          url: `https://gitlab.com/RecapTime/verify/issues`,
          disabled: false,
          type: 2,
        },
        {
          style: 5,
          label: `Learn more in project's README`,
          url: `https://gitlab.com/RecapTime/veriy`,
          disabled: false,
          type: 2,
        },
      ],
    },
  ],
  embeds: [
    {
      type: "rich",
      title: `Instructions on how do you get Verified role in Recap Time Squad's server + services`,
      description: "",
      color: 0x00ff73,
      fields: [
        {
          name: `1. Press the button below to get the GitHub issue form.`,
          value: `In case GitHub is down or our GitHub app is having some hicupps on copying issues, you can directly [submit on GitLab here](https://gitlab.com/RecapTime/verify/issues/new) and select \`Add new entry\` issue template.`,
        },
        {
          name: `2. Read instructions as prompted. Any incomplete issue will be then closed without notice by any of the maintainers.`,
          value: `In GitLab, instructions are written as HTML comments, so please read them carefully. We're working on an webapp to simplify this workflow pretty soon.`,
        },
        {
          name: `3. Once you're satisified, you can hit the \"Create issue\" button`,
          value: `**REMEMBER TO CHECK IF THERE ARE ANY EXISTING OPEN ISSUES!** If there are any, even close one, make sure to stay in the loop. You can also submit an merge request then. (We require each MRs to have its own issue for tracking purposes.)`,
        },
      ],
    },
  ],
};

if (!context.params.event.user) {
  await lib.discord.interactions["@1.0.0"].responses.ephemeral.create({
    // we send an ephemeral message to the user confirming that the responses have been logged
    token: `${context.params.event.token}`,
    content: `<@!${context.params.event.member.user.id}>`,
    tts: false,
    embeds: [
      {
        type: "rich",
        title: `Check your DMs for instructions!`,
        description: `If you don't allow server members to DMs you, including bots, please adjust your privacy settings for this server.`,
        color: 0x9900ff,
        image: {
          url: `https://i.imgur.com/vbPYlw1.png`,
          height: 0,
          width: 0,
        },
      },
    ],
    response_type: "CHANNEL_MESSAGE_WITH_SOURCE",
  });
  await lib.discord.users["@0.2.0"].dms.create({
    recipient_id: `${context.params.event.member.user.id}`,
    content: msgTemplate.content,
    embeds: msgTemplate.embeds,
    components: msgTemplate.components,
  });
} else {
  await lib.discord.channels["@0.3.1"].messages.create({
    channel_id: `${context.params.event.channel_id}`,
    content: msgTemplate.content,
    embeds: msgTemplate.embeds,
    components: msgTemplate.components,
  });
}
