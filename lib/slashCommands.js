module.exports = {
  verify: {
    name: "verify",
    description:
      "Get you, your community or your company verified across Recap Time Squad's apps and through the API.",
    options: [],
  },
  connectGit: {
    name: "connect-git",
    description:
      "Connect your Discord account to your GitHub/GitLab SaaS username. Rerun this when you change them.",
    options: [
      {
        type: 3,
        name: "host",
        description:
          "Git host we'll use to associate your CLA signature to your Discord account, among other things.",
        choices: [
          {
            name: "GitHub",
            value: "github.com",
          },
          {
            name: "GitLab SaaS",
            value: "gitlab.com",
          },
        ],
        required: true,
      },
    ],
  },
  userinfo: {
    name: "userinfo",
    description: "Query Discord user against the API for any associations.",
    options: [
      {
        type: 6,
        name: "target_user",
        description:
          "Looks up either the mentioned Discord user or you against the API for any connected entries.",
      },
    ],
  },
  status: {
    name: "status",
    guild_id: `${process.env.DISCORD_GUILD_ID}`,
    description: "Set's your bot's status",
    options: [
      {
        type: 3,
        name: "status",
        description: "Set the bot's status on Discord",
        choices: [
          {
            name: "Online",
            value: "ONLINE",
          },
          {
            name: "Idle",
            value: "IDLE",
          },
          {
            name: "Do Not Disturb",
            value: "DND",
          },
          {
            name: "Offline",
            value: "INVISIBLE",
          },
        ],
        required: true,
      },
      {
        type: 3,
        name: "type",
        description: "Set the bot's activity type.",
        choices: [
          {
            name: "Playing",
            value: "GAME",
          },
          {
            name: "Watching",
            value: "WATCHING",
          },
          {
            name: "Listening to",
            value: "LISTENING",
          },
          {
            name: "Competing in",
            value: "COMPETING",
          },
          {
            name: "Streaming",
            value: "STREAMING",
          },
        ],
        required: true,
      },
      {
        type: 3,
        name: "name",
        description: "Set the name of your activity",
        required: true,
      },
      {
        type: 3,
        name: "url",
        description:
          "The URL your streaming status will link to. (Works on Streaming status only)",
      },
    ],
  },
  updateCommands: {
    name: "update-commands",
    description:
      "Refreshes slash commands for this guild. Requires Manage Server perm to do this.",
    options: [],
  },
};
