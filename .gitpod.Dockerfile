FROM gitpod/workspace-full:latest

RUN bash -lc "source ~/.nvm/nvm-lazy.sh && nvm install --lts && nvm use --lts && nvm install-latest-npm" \
  && npm i -g lib.cli \
  && brew install shellcheck hadolint direnv
