// authenticates you with the API standard library
const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });

let messageResponse = await lib.discord.channels["@0.0.6"].messages.create({
  channel_id: `${context.params.event.channel_id}`,
  content: [
    `Hey <@!${context.params.event.author.id}>! I'm a bot powered by Autocode, with love by the Recap Time Squad.`,
    `Currently, most of the commands are only available via slash commands to simplify our workflow.`,
  ].join("\n"),
  embed: {
    title: "About the bot",
    type: "rich",
    color: 0x00aa00, // Green color
    description:
      "We built this bot for Discord, among other bots elsewhere, to compliment our verification process for both creators and communities on our platforms in an open-source way.",
    fields: [
      {
        name: "Where is the source code?",
        value: [
          "We're working on to make it open-source outside Autocode pretty soon. You could ask us to give you access to the project",
          "and also help us bring even more features to the bot.",
        ].join(" "),
      },
      {
        name: "Link your Discord to GitHub/GitLab SaaS username",
        value: [
          "Check out the documentation on how you can",
          "https://autocode.com/app/discord/basic-discord-example/",
        ].join("\n"),
      },
    ],
  },
  tts: false,
});

return messageResponse;
