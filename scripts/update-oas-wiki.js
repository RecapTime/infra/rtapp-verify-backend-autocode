#!/usr/bin/env node
const { readFileSync } = require('fs');
const axios = require("axios")
const apiBase = axios.todo;

var yamlData = readFileSync("./www/openapi.yaml", 'utf8')
var encodedData = encodeURIComponent(yamlData)
console.log("URL-encoded YAML data:", encodedData)