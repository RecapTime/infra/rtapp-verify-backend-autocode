/**
 * Environment variable loader from our env.json file (omitted from GitLab code export due to potential leakage
 * of secrets). It is usually loaded at top of most of the functions and usually used at local development and
 * when doing API development using Gitpod/containers.
 */
function loadEnvFromFile() {
  const fs = require("fs");
  const env = require("../env.json")
  if (process.env.LD == "/opt/node_modules/autocode-common/configs") {
    // noop
  } else {
    Object.keys(env.dev).forEach(function (key) {
      process.env[key] = env.dev[key]
    })
  }
}

module.exports = {
  loadEnvFromFile,
};
