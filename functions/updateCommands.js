const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });
const slashCommandsConfig = require("../lib/slashCommands");

try {
  /** After all of these, public commands go here **/
  gitConnect = await lib.discord.commands["@0.0.0"].create({
    name: slashCommandsConfig.connectGit.name,
    description: slashCommandsConfig.connectGit.description,
    options: slashCommandsConfig.connectGit.options,
  });
  verify = await lib.discord.commands["@0.0.0"].create({
    name: slashCommandsConfig.verify.name,
    description: slashCommandsConfig.verify.description,
  });
  userLookup = await lib.discord.commands["@0.0.0"].create({
    name: slashCommandsConfig.userinfo.name,
    description: slashCommandsConfig.userinfo.description,
    options: slashCommandsConfig.userinfo.options,
  });
  updateCommands = await lib.discord.commands["@0.0.0"].create({
    name: slashCommandsConfig.updateCommands.name,
    description: slashCommandsConfig.updateCommands.description,
  });

  return {
    statusCode: 200,
    body: {
      ok: true,
      results: [gitConnect, verify, userLookup, updateCommands],
    },
  };
} catch (err) {
  console.warn(err);
  return {
    statusCode: 500,
    body: {
      ok: false,
      error: "Something went wrong.",
      traceback: err,
    },
  };
}
