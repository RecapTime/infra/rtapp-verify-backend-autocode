/**
 * An HTTP endpoint that acts as a webhook for GitHub issues event
 * @param {object} event
 * @returns {any} result
 */
module.exports = async (event, context) => {
  // authenticates you with the API standard library
  // type `await lib.` to display API autocomplete
  const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });

  if (
    context.params.event.repository.owner.login == process.env.GH_NAMESPACE &&
    context.params.event.repo == process.env.GH_REPO_SLUG
  ) {
    const issueMetadata = lib.github.issues["@0.3.5"].retrieve({
      owner: `${event.repository.owner.login}`,
      repo: `${event.repository.name}`,
      issue_number: Number(`${event.issue.id}`),
    });
    const issueBody = issueMetadata.body;
    let glApiResponse = lib.http.request["@1.1.6"].post({
      url: `https://${process.env.GITLAB_HOST}/api/v3/projects/${process.env.GITLAB_PROJECT_ID}/issues`,
      authorization: `${process.env.GITLAB_TOKEN}`,
      params: {
        title: `todo`,
        body: `todo`,
      },
    });
    let closeThis = lib.github.issues["@0.3.5"].update({
      owner: `${event.repository.owner.login}`,
      repo: `${event.repository.name}`,
      issue_number: Number(`${event.issue.number}`),
      state: "closed",
    });

    return {
      github: closeThis,
      gitlab: glApiResponse,
    };
  } else {
    return {
      ok: false,
      error: "Repo name and owner login mismatch found!",
    };
  }
};
