/**
 * Any changes in this file will cause additional API calls to the Slash Commands API on Discord side.
 * To propagate changes now, press the Run button. By default, they'll be propagated when somebody installs the app
 * into their server. If you want to add new commandshere, use https://autocode.com/tools/discord/command-builder/
 * to easily generate the code needed.
 */
const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });
const slashCommandsConfig = require("../../../../lib/slashCommands");

/** Internal commands go here **/
status = await lib.discord.commands["@0.0.0"].create(
  slashCommandsConfig.status
);

/** After all of these, public commands go here **/
gitConnect = await lib.discord.commands["@0.0.0"].create(
  slashCommandsConfig.connectGit
);
verify = await lib.discord.commands["@0.0.0"].create(
  slashCommandsConfig.verify
);
userLookup = await lib.discord.commands["@0.0.0"].create(
  slashCommandsConfig.userinfo
);
updateCommands = await lib.discord.commands["@0.0.0"].create(
  slashCommandsConfig.updateCommands
);

return {
  results: [status, gitConnect, verify, userLookup, updateCommands],
};
