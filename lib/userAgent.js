module.exports = () => {
    if (!process.env.USER_AGENT_HTTP_CLIENT) {
        const { name, version, repository }= require("../package.json")
        return `${name}@${version}, ${repository.url}`
    } else {
        return process.env.USER_AGENT_HTTP_CLIENT
    }
}